FROM python:3
WORKDIR /usr/src/app/templates
COPY /templates/index.html .
COPY /templates/result.html .
WORKDIR /usr/src/app/
COPY app.py .
COPY requirements.txt .
RUN pip3 install -r requirements.txt
EXPOSE 5000
CMD [ "python", "app.py", "runserver", "0.0.0.0:80" ]
